#!/bin/bash

echo "Creating tarball for Geant4 v${SOURCE_UVERSION}"

# archive names (http://geant4.web.cern.ch/support/download)
G4NDL_TAR_GZ="G4NDL.${G4NDL_VERSION}.tar.gz"
G4EMLOW_TAR_GZ="G4EMLOW.${G4EMLOW_VERSION}.tar.gz"
G4PhotonEvaporation_TAR_GZ="G4PhotonEvaporation.${G4PhotonEvaporation_VERSION}.tar.gz"
G4RadioactiveDecay_TAR_GZ="G4RadioactiveDecay.${G4RadioactiveDecay_VERSION}.tar.gz"
G4SAIDDATA_TAR_GZ="G4SAIDDATA.${G4SAIDDATA_VERSION}.tar.gz"
G4PARTICLEXS_TAR_GZ="G4PARTICLEXS.${G4PARTICLEXS_VERSION}.tar.gz"
G4ABLA_TAR_GZ="G4ABLA.${G4ABLA_VERSION}.tar.gz"
G4INCL_TAR_GZ="G4INCL.${G4INCL_VERSION}.tar.gz"
G4PII_TAR_GZ="G4PII.${G4PII_VERSION}.tar.gz"
G4ENSDFSTATE_TAR_GZ="G4ENSDFSTATE.${G4ENSDFSTATE_VERSION}.tar.gz"
G4RealSurface_TAR_GZ="G4RealSurface.${G4RealSurface_VERSION}.tar.gz"
G4TENDL_TAR_GZ="G4TENDL.${G4TENDL_VERSION}.tar.gz"

# prepare download folder
DATASET_DOWNLOAD_DIR="$(dirname $0)/../../geant4-data_tarball"
mkdir -p ${DATASET_DOWNLOAD_DIR}
cd ${DATASET_DOWNLOAD_DIR}
rm -f geant4-data_${SOURCE_DVERSION}_files
rm -f geant4-data_${SOURCE_DVERSION}.tar.gz

# prepare source folder
ORIG_SOURCE_DIR="geant4-data_${SOURCE_DVERSION}"
rm -rf ${ORIG_SOURCE_DIR}
mkdir -p ${ORIG_SOURCE_DIR}

# download archives
echo "Downloading archives"
DATASET_BASE_URL="http://cern.ch/geant4-data/datasets/"
echo "
${DATASET_BASE_URL}${G4NDL_TAR_GZ}
${DATASET_BASE_URL}${G4EMLOW_TAR_GZ}
${DATASET_BASE_URL}${G4PhotonEvaporation_TAR_GZ}
${DATASET_BASE_URL}${G4RadioactiveDecay_TAR_GZ}
${DATASET_BASE_URL}${G4SAIDDATA_TAR_GZ}
${DATASET_BASE_URL}${G4PARTICLEXS_TAR_GZ}
${DATASET_BASE_URL}${G4ABLA_TAR_GZ}
${DATASET_BASE_URL}${G4INCL_TAR_GZ}
${DATASET_BASE_URL}${G4PII_TAR_GZ}
${DATASET_BASE_URL}${G4ENSDFSTATE_TAR_GZ}
${DATASET_BASE_URL}${G4RealSurface_TAR_GZ}
${DATASET_BASE_URL}${G4TENDL_TAR_GZ}
" | xargs -n 1 -P 12 wget -nv -N

# extract to source folder
echo "Extracting archives"
tar -C ${ORIG_SOURCE_DIR} -xf ${G4NDL_TAR_GZ}
tar -C ${ORIG_SOURCE_DIR} -xf ${G4EMLOW_TAR_GZ}
tar -C ${ORIG_SOURCE_DIR} -xf ${G4PhotonEvaporation_TAR_GZ}
tar -C ${ORIG_SOURCE_DIR} -xf ${G4RadioactiveDecay_TAR_GZ}
tar -C ${ORIG_SOURCE_DIR} -xf ${G4SAIDDATA_TAR_GZ}
tar -C ${ORIG_SOURCE_DIR} -xf ${G4PARTICLEXS_TAR_GZ}
tar -C ${ORIG_SOURCE_DIR} -xf ${G4ABLA_TAR_GZ}
tar -C ${ORIG_SOURCE_DIR} -xf ${G4INCL_TAR_GZ}
tar -C ${ORIG_SOURCE_DIR} -xf ${G4PII_TAR_GZ}
tar -C ${ORIG_SOURCE_DIR} -xf ${G4ENSDFSTATE_TAR_GZ}
tar -C ${ORIG_SOURCE_DIR} -xf ${G4RealSurface_TAR_GZ}
tar -C ${ORIG_SOURCE_DIR} -xf ${G4TENDL_TAR_GZ}

# create list of files (due to empty folders)
find geant4-data_${SOURCE_DVERSION}/* -type f > geant4-data_${SOURCE_DVERSION}_files

# create new archive
echo "Building new archive"
tar -czf geant4-data_${SOURCE_DVERSION}.orig.tar.gz --files-from=geant4-data_${SOURCE_DVERSION}_files --strip-components=1

echo "Created geant4-data_${SOURCE_DVERSION}.orig.tar.gz"
