#!/usr/bin/make -f

# The dataset files for Geant4 can not be nicely fetched, since their URL is
# only accessible when javascript is run. Thus, the versions need to be
# extraced by hand from https://geant4.web.cern.ch/support/download.

# geant4 version
export SOURCE_UVERSION=11.0.1
export SOURCE_DVERSION=${SOURCE_UVERSION}+ds

# dataset versions
export G4NDL_VERSION=4.6
export G4EMLOW_VERSION=8.0
export G4PhotonEvaporation_VERSION=5.7
export G4RadioactiveDecay_VERSION=5.6
export G4SAIDDATA_VERSION=2.0
export G4PARTICLEXS_VERSION=4.0
export G4ABLA_VERSION=3.1
export G4INCL_VERSION=1.0
export G4PII_VERSION=1.3
export G4ENSDFSTATE_VERSION=2.3
export G4RealSurface_VERSION=2.2
export G4TENDL_VERSION=1.4
